# Introduction

Users of IsardVDI are classified in [roles](../manager/users.md#roles). This section describes features that can be used byadminitrator role users.

Users with **administator role** have the features of all [users](../user/index.md), [advanced users](../advanced/index.md), [manager users](../manager/index.md) and those described in this section.
