# Multitenancy

The quotas can limit the resources used by each role, category, group or user individually for different items.

## Introduction

Multitenancy IsardVDI installation allows for multiple organizations/companies to share the same IsardVDI installation. This is, the same infrastructure now can be *split* by *limits* into a shared infrastructure, where *managers* in that organization can administer everything within the defined limits.

![](../images/admin/mt_description.png)

This allows a new level of control as categories and groups in categories can have limits stablished (users, desktops, vCPUs, ...).

## Configuration

In **users** menu [administrators](index.md) can manage categories that can have a **manager user** with full control over that category.

To create a new category with resource usage limits and user autoregistering (github/google) as an [administrator](index.md) user:

- **Add new category** and set a name to it. For example we will create*ACME* category. 

  ![](../images/admin/mt_category.png)

  A new *Main* group with this parent category will be also added in the groups.

  ![](../images/admin/mt_group.png)

You can create as many groups in that category as you want like [Managers](../manager/users.md#add-group).

- **Set category limits** by opening the category details and click in the *Limits* button.

  ![](../images/admin/mt_limits.png)

You can set here *quotas* and *limits* for users in this category like by [Groups](../manager/users.md#quotas).

- **Add new manager user** by clicking in the users section and be sure to set his role to *manager* and the category to **ACME** and group to **Main ACME**

  ![](../images/admin/mt_manageruser.png)

- **Get autoregister OAuth codes** [like an manager can do it](../manager/users.md#add-group).

Now you have configured the *ACME* company with a [manager](../manager/index.md) and set limits to it. The users won't have any quota limits so you should.
