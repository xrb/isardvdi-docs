# Introducció

Els usuaris d'IsardVDI estan classificats en [rols](../manager/users.md#roles). Aquesta secció descriu funcions que poden ser utilitzades pels usuaris del rol de mànager.

Els usuaris amb **rol de mànager** tenen les funcionalitats de tots els [usuaris](../user/index.ca.md) i dels [usuaris avançats](../advanced/index.ca.md) a més de les descrites en aquesta secció.
