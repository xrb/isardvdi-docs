# Códigos de registro

Es posible acceder a IsardVDI a través de una cuenta de Google, para poder acceder es necesario un código de registro, este lo podemos obtener accediendo a la interfaz avanzada.

Una vez accedemos a la interfaz avanzada, en el menú de la izquierda vamos a la opción "Users".

![](./create_enrollment_codes.es.images/google12.png)

A continuación nos desplazaremos hasta la opción de "Groups".

![](./create_enrollment_codes.es.images/google5.png)

Pulsaremos el icono
![](./create_enrollment_codes.es.images/google13.png)
sobre el grupo en el que tenemos a los usuarios.


Pulsaremos sobre el icono 
![](./create_enrollment_codes.es.images/google7.png)
y nos abrirá una nueva ventana donde podremos escojer sobre que roles queremos crear un código de registro.

![](./create_enrollment_codes.es.images/google8.png)

Una vez obtenidos, podemos proporcionale el código al usuario que queramos que acceda mediante Google, y solo deberá utilizar el código la primera vez que se [autentique con Google](../user/google_account.es).

