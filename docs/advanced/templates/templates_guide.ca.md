# Treballar amb plantilles

La intenció d'aquesta guia és mostrar com crear plantilles a partir d'un escriptori, com gestionar les accions d'una plantilla i com compartir la plantilla amb altres usuaris del sistema.

Una plantilla és a IsardVDI un concepte bàsic en l'esquema de creació d'escriptoris. Una plantilla permet establir un estat a un escriptori de manera que ja no serà modificat de nou. A partir d'aquest moment es podran crear escriptoris basats en aquesta plantilla i que seran idèntics a l'estat en el qual es va deixar la plantilla però a partir d'ara aquests nous escriptoris es podran continuar instal·lant i configurant amb els aplicatius que es desitgi.

Per tant una plantilla es crearà sempre a partir d'un escriptori, *congelant* així l'estat en el qual es trobava l'escriptori i poder generar a partir d'aquesta nova plantilla nous escriptoris idèntics.

**ATENCIÓ**: Revisi a consciència l'estat de l'escriptori abans de convertir-lo a una plantilla. La plantilla que es creï serà l'escriptori en l'estat en el qual la deixi i altres usuaris crearan escriptoris idèntics a aquesta plantilla. Per tant <u>NO DEIXI CAP INFORMACIÓ PERSONAL</u> emmagatzemada a l'escriptori del qual es crearà la plantilla ja que estarà també als escriptoris que posteriorment es creïn a partir d'aquesta plantilla. Revisi la *cache* dels navegadors usats, les dades personals introduïdes en els navegadors, qualsevol fitxer descarregat o clau que pugui haver introduït. Si va accedir al correu electrònic des d'aquest escriptori revisi que no van quedar emmagatzemades en el navegador les seves credencials. En cas de dubte no converteixi l'escriptori en plantilla o bé asseguri's de no compartir-la mai amb altres usuaris.

Perquè es comprengui millor la relació entre escriptoris i plantilles vegem el següent esquema. Prengui's el seu temps per a entendre les relacions aquí explicades ja que serà clau per a entendre la resta de conceptes.

![](./images/derivates.jpg)

Inicialment es va crear un escriptori **Ubuntu 16* que es va convertir en una plantilla que és la que veiem a l'esquema. Aquesta plantilla es va compartir per tal que altres usuaris poguessin crear un escriptori idèntic al d'aquesta plantilla.

Altres usuaris (professors a l'exemple) que tenien accés a aquesta plantilla es van crear els seus escriptoris, *Arduino* i *gimp* a partir de la plantilla *Ubuntu 16*. Aquests escriptoris inicialment només tenien el sistema operatiu i els usuaris (professors) van decidir, un instal·lar dins programari d'arduino per als seus alumnes i l'altre va instal·lar en el seu el programari d'edició d'imatges gimp. Una vegada van tenir els seus escriptoris amb el programari que necessitaven ho van convertir en plantilles que van compartir amb altres usuaris (amb els seus alumnes en aquest exemple).

Finalment altres usuaris (alumnes en aquest exemple) van crear els seus respectius escriptoris idèntics a les plantilles d'*arduino* i/o de *gimp*, on van començar a treballar amb el programari ja instal·lat.

Per tant veiem que hi ha una relació directa entre els escriptoris finals creats pels usuaris i les plantilles de les quals van derivar. Existeix un arbre que es genera a mesura que es van creant nous escriptoris a partir de plantilles.

Entendre aquest concepte és clau per a entendre com treballar amb IsardVDI i el perquè del resultat de certes accions que realitzarem amb les plantilles en els següents apartats.

## Crear una plantilla

Per a crear una plantilla haurem de partir d'un escriptori que no estigui arrencat. Revisi una vegada més que no ha deixat informació personal a l'escriptori abans de crear una plantilla.

Obrirem els detalls de l'escriptori mitjançant el botó blau '+' i trobarem allí el botó de *Template it* que ens obrirà el formulari per a crear la plantilla.

![](./images/templateit.png)

En aquest formulari només és requisit establir un nom (que no haurà de ser el mateix que ja té l'escriptori) per a la nova plantilla que es crearà.

![](./images/template_form.png)

No canviï el tipus de plantilla, sempre usi el tipus *Template*.

Abans de confirmar la creació de la plantilla podríem modificar el seu maquinari (*Hardware*) o els mitjans afegits (*Media*), encara que és poc habitual que es necessiti una plantilla amb aquests paràmetres diferents dels que estàvem usant per a l'escriptori del qual es crearà. Si es tracta d'un escriptori que hem instal·lat des d'una ISO és possible que aquesta encara estigui afegida als mitjans i no vulguem que passi també a la plantilla, amb el que pot ser interessant treure-la ara del desplegable de mitjans (*Media*).

També podrem establir els permisos de compartició (*Allows*) amb altres rols, categories, grups o usuaris en aquest moment si ho desitgem. Si no els modifiquem per defecte la plantilla que es crearà no es compartirà amb ningú, amb la qual cosa parlarem d'una plantilla privada (només serà visible per a nosaltres). Posteriorment podrem editar de nou la compartició de la plantilla per a modificar qui té accés si ho desitgem.

Una vegada revisats els paràmetres podrem passar a crear la plantilla amb el botó *Create template*. És interessant tenir clar què farà aquest procediment per a entendre com funciona el sistema de plantilles. Comprovem en el menú de plantilles (*Templates*) que s'ha creat la plantilla.

![](./images/templates.png)

Veiem que s'ha creat la plantilla *Template Windows 10 pro* i que, com no hem modificat els permisos de compartició (*Alloweds*) per defecte, només és accessible per nosaltres i ens ho indica com una plantilla privada (*Private*).

### Com IsardVDI converteix l'escriptori a una plantilla

Hem explicat que l'escriptori quedarà *congelat* en l'estat en el qual estava com la nova plantilla que hem creat. El que ha succeït és que el disc de l'escriptori ha passat a ser el disc de la nova plantilla. Per això  recalquem de nou que és importantíssim revisar que no quedin dades personals o credencials en l'escriptori del qual es crearà la plantilla.

Una vegada el disc s'ha mogut a la nova plantilla el sistema crearà un nou escriptori basat en aquesta plantilla amb el mateix nom de l'escriptori que ja teníem. Tot el que es faci a partir d'ara en aquest escriptori és independent de la plantilla, que ja va quedar congelada en crear-se.

En aquest diagrama de flux veiem el que succeeix quan decidim convertir l'escriptori en una plantilla:

![](./images/template_secuence.png)

Això és el que succeeix en els passos:

1. Escollim un escriptori des del que crearem una nova plantilla. Usarem el botó *template it* que ens apareix en obrir els detalls de l'escriptori (signe + blau a  l'esquerra). Emplenem un nom en el formulari de creació de plantilla o bé, com es mostra en la imatge, hem acceptat les dades que ens proposa el sistema per defecte.
2. Aquesta operació ha creat una plantilla amb el nom indicat *Template Windows 10 pro* que trobarem en el menú de *Templates* a l'esquerra. Aquesta plantilla, si no hem modificat els permisos (*Alloweds*) ens indicarà que és privada. Això vol dir que ara com ara només  nosaltres podrem crear nous escriptoris idèntics a l'estat actual en el  qual es va crear la plantilla.
3. IsardVDI automàticament ens haurà creat de nou el nostre escriptori *Windows 10 pro*. I diem que ens haurà creat perquè el disc que usava s'ha mogut a la plantilla i per a tornar a tenir un escriptori idèntic i  estalviar-nos haver de crear-lo nosaltres, IsardVDI ho ha fet automàticament.

Cal tenir en compte que aquest nou escriptori depèn ara de la plantilla en quant a que si eliminem la plantilla se'ns indicarà que també ha de ser eliminat aquest escriptori (i la resta d'escriptoris o plantilles que s'hagin pogut derivar d'aquesta).

## Compartir una plantilla

Podrem decidir si altres usuaris poden crear un escriptori idèntic a una plantilla donant-los accés a aquesta plantilla mitjançant el botó d'usuaris de la dreta, en la columna de compartició (*Shares*). Aquest botó obrirà el formulari de compartició. Una plantilla compartida farà que els usuaris que hi tinguin accés la tinguin visible en el seu formulari d'afegir nou escriptori.

![](./images/template.png)

En aquest formulari podrem escollit entre compartir amb tots els usuaris del nostre sistema o bé només amb alguns d'ells, individualment o bé amb els grups, categories o rols d'aquests usuaris.

![](./images/templates_allowed.png)

Com veiem a la imatge anterior aquesta plantilla no està compartida amb ningú ja que no està activada cap compartició. Aquestes serien les regles bàsiques per a establir comparticions d'una plantilla:

- **No compartir amb cap usuari del sistema** (plantilla privada): No haurà d'estar activada cap casella d'aquest formulari. La plantilla només estarà disponible perquè nosaltres creiem escriptoris derivats d'ella.

![](./images/allowed_none.png)

- **Compartir amb tots els usuaris del sistema**: Si desitgem que qualsevol usuari del sistema pugui crear un escriptori a partir de la nostra plantilla haurem d'activar una casella qualsevol (la d'establir rols per exemple, *Set roles*) i deixar en blanc el camp de cerca associat (el de rols per exemple, *Roles*).

![](./images/allowed_all.png)

- **Compartir amb alguns usuaris**: Podrà establir la compartició amb alguns usuaris individualment o bé amb tots els usuaris d'un grup, categoria o amb un rol determinat.
  - Amb usuaris individuals: Seleccioni establir usuaris (*Set users*) i escrigui almenys dos caràcters en el camp de cerca associat (*Users*)
  - Amb tots els usuaris de grups: Seleccioni establir grups (*Set groups*) i escrigui almenys dos caràcters en el camp de cerca associat (*Groups*)
  - Amb tots els usuaris de categories: Seleccioni establir categories (*Set categories*) i escrigui almenys dos caràcters en el camp de cerca associat (*Categories*)
  - Amb tots els usuaris que tinguin els rols: Seleccioni establir rols (*Set roles*) i escrigui almenys dos caràcters en el camp de cerca associat (*Roles*). Actualment els rols del sistema predefinits són *Administrators*, *Managers*, *Advanced* i *Users*.

Tingui en compte respecte a les plantilles també que:

- En els camps de cerca podrà afegir múltiples valors als quals s'aplicarà.

- Si activa i selecciona més d'un valor i tipus la compartició s'aplicarà als usuaris que apliquin a totes elles. Per exemple en el següent exemple:

![](./images/allowed_mixed.png)

- Si activa un tipus i deixa en blanc el camp de cerca associat (o deixa qualsevol camp de cerca en blanc i activat) la plantilla quedarà compartida amb tots els usuaris del sistema independentment del que seleccioni en la resta de tipus.
- Mentre una plantilla estigui compartida amb usuaris aquests podran crear un escriptori derivat d'aquesta. El fet de tornar a convertir la plantilla en privada no deshabilitarà ni eliminarà els escriptoris que ja s'hagin creat. Per a eliminar els escriptoris derivats i/o la plantilla l'usuari propietari de la plantilla haurà de contactar amb el gestor de la categoria (*manager*) o bé amb l'administrador de tot el sistema. Això és així per a assegurar una comprovació d'aquesta operació per part d'un usuari amb permisos. Recordi l'esquema en arbre de plantilles i escriptoris que serien eliminats en cadena com a resultat.
