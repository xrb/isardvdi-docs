# Local client

Users can use a browser to view desktops but with a local client could enjoy more features such as connecting USB devices.

## How to install local client for SPICE

### Linux

On Linux, you need to run the following commands:

#### Debian and derivatives like Ubuntu:

```
sudo apt install virt-viewer
```

#### RedHat and derivatives like CentOS and Fedora

```
sudo dnf install remote-viewer
```

### Windows

On Windows, you need to download and install one of the following:

#### Windows 64bits (Windows 10)

- SPICE viewer: <https://isardvdi.com/virt-viewer-x64.msi>
- SPICE USB: <https://isardvdi.com/usbdk-x64.msi>

#### Windows 32bits (other Windows versions)

- SPICE viewer: <https://isardvdi.com/virt-viewer-x86.msi>
- SPICE USB: <https://isardvdi.com/usbdk-x86.msi>

#### Step by step on Windows 10

Open <https://isardvdi.com/virt-viewer-x64.msi> with Firefox. A windows is opened, click Save File button.
![](local-client-images/20201223_09h34m48s_grim.png)

Click blue down arrow ![](local-client-images/20201223_09h36m17s_grim.png)

![](local-client-images/20201223_09h36m11s_grim.png)

A menu is shown, click Virt Viewer MSI file.

![](local-client-images/20201223_09h36m35s_grim.png)

A windows is opened, click OK button.

![](local-client-images/20201223_09h36m49s_grim.png)

User Account Control dialog is opened, click Yes button.

![](local-client-images/20201223_09h37m13s_grim.png)

A windows is opened with the installation progress.

![](local-client-images/20201223_09h37m28s_grim.png)

Proceed installing UsbDk:

Open <https://isardvdi.com/usbdk-x64.msi> with Firefox (Edge doesn't download it correctly). A windows is opened, click Save File button.

![](local-client-images/20201223_09h38m08s_grim.png)

Click blue down arrow ![](local-client-images/20201223_09h36m17s_grim.png)

![](local-client-images/20201223_09h36m11s_grim.png)

A menu is opened, click UsbDk MSI file.

![](local-client-images/20201223_09h38m41s_grim.png)

A windows is opened, click OK button.

![](local-client-images/20201223_09h39m12s_grim.png)

User Account Control dialog is opened, click Yes button.

![](local-client-images/20201223_09h39m43s_grim.png)

A windows is opened with the installation progress.

![](local-client-images/20201223_09h40m07s_grim.png)

Once Virt Viwer and UsbDk are installed you could go to IsardVDI web interface and use Local client button.
A window is opened, click OK button to open the virtual desktop with remote-viewer.

![](local-client-images/20201223_10h49m16s_grim.png)

A window is opened with the virtual desktop inside.

## Connect an USB device

Once you have installed SPICE local client Virt Viewer (see above sections to install it), you could connect an USB device to a virtual desktop.

Open file menu and select "USB device selection".

![](local-client-images/20201223_10h50m23s_grim.png)

A windows is opened with your USB devices list. Connect your USB device to the computer and it will appear in the list. Click the check box corresponding to desired device.

![](local-client-images/20201223_10h50m57s_grim.png)
