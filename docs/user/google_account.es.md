# Acceso mediante Google

Desde la página principal es posible acceder mediante una cuenta de correo de Google.

![](./google_account.es.images/google1.png)

Pulsamos sobre el icono
![](./google_account.es.images/google9.png)
de la pantalla principal de IsardVDI 

Nos solicitara una cuenta de correo de Google 
![](./google_account.es.images/google2.png)

Agregamos la dirección de correo y a continuación pulsamos sobre el icono siguiente y nos solicitara la contraseña de la cuenta de Google.
![](./google_account.es.images/google4.png)

Una vez introducida volvemos a pulsar sobre el icono siguiente. Si es la primera vez nos aparecera el formulario de registo.
![](./google_account.es.images/google3.png)

El codigo de registro, nos lo deben proporcionar los gestores de la organización.

Introduciremos el codigo que nos han proporcionado y pulsamos sobre el icono
![](./google_account.es.images/google11.png)

Accediendo a la interfaz básica autenticados con la cuenta de Google.
![](./basic_interface.es.images/interface7.png)

Una vez autenticados, ya no requeriremos repetir el proceso de registro.
Cuando queramos volver a acceder mediante Google, solo debermos pulsar sobre el icono
![](./google_account.es.images/google9.png)
y utilizar las credenciales de Google.

