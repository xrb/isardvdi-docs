# Accés al escriptori mitjançant un enllaç

Per generar l'enllaç, haurem de accedir a la opció "desktops".

![](./link_desktop_access.en.images/captura1.png)

Triarem el "desktop" del qual volem generar-lo i polsarem sobre la següent icona:

![](./link_desktop_access.en.images/captura2.png)

En el desplegable que s'obre a continuació polsarem sobre la icona "viewer":

![](./link_desktop_access.en.images/captura3.png)

![](./link_desktop_access.en.images/captura4.png)

En la nova finestra que s'obre ens apareixerà l'enllaç per tal de poder accedir al "desktop".

![](./link_desktop_access.en.images/captura5.png)

Per copiar el enllaç cal polsar sobre el boto de  "copy to clipboard" ja que la opció de copiar el text apareix deshabilitada.

![](./link_desktop_access.en.images/captura6.png)

Una vegada copiat, obrim una finestra en el nostre navegador enganxem la URL i ens portara a la següent finestra:

![](./link_desktop_access.en.images/captura7.png)

Aquest enllaç es pot enviar a qualsevol usuari per tal de poder accedir al escriptori sense tindre que repetir aquest procés ja que aquest enllaç no caduca.

