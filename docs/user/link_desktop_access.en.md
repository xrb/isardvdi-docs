# Access to the desktop via a link

In order to generate the link, we will have to access the option desktops.

![](./link_desktop_access.en.images/captura1.png)

Choose the desktop from which you want to generate the link and click on the following icon:

![](./link_desktop_access.en.images/captura2.png)

Click on the viewer icon in the drop-down menu that appears:

![](./link_desktop_access.en.images/captura3.png)

![](./link_desktop_access.en.images/captura4.png)

The link to access the desktop will appear in the new window that opens.

![](./link_desktop_access.en.images/captura5.png)

To copy it, click on the copy to clipboard icon, as the option to copy the text is disabled.

![](./link_desktop_access.en.images/captura6.png)

Once copied, open a browser window, paste the URL and the following window will open:

![](./link_desktop_access.en.images/captura7.png)

This link can be sent to any user in order to access the desktop without having to repeat this process as the generated link does not expire.

