# Introducció

Els usuaris d'IsardVDI estan classificats en [rols](../manager/users.md#roles). Aquesta secció descriu funcions que poden ser utilitzades per tots els rols.

Comenceu a aprendre com podeu accedir als escriptoris virtuals a [Web interfaces](webinterfaces.md).
