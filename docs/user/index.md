# Introduction

Users of IsardVDI are classified in [roles](../manager/users.md#roles). This section describes features that can be used by all roles.

Start learning how you can access to the Virtual Desktops at [Web interfaces](webinterfaces.md).
